var game = new Phaser.Game(320, 480, Phaser.AUTO, 'game_div');
var game_state = {};

game_state.main = function() {};  
game_state.main.prototype = {
    preload: function() { 
        this.game.stage.backgroundColor = '#000000';
        this.game.load.spritesheet('bird', 'assets/bird.png', 64, 64);
        this.game.load.image('cloud1', 'assets/cloud1.png');
        this.game.load.image('cloud2', 'assets/cloud2.png');
        this.game.load.image('wall', 'assets/wall.png');
        this.game.load.image('background', 'assets/bground.png');
        this.game.load.image('ground', 'assets/ground.png');
        this.game.load.audio('bgroundMusic', ['assets/joyful.mp3']);
        this.game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL; //resize your window to see the stage resize too
        this.game.stage.scale.setShowAll();
        this.game.stage.scale.refresh();

    },

    create: function() { 
    	this.background = this.game.add.tileSprite(0, 0, 512, 512, 'background');
    	this.ground = this.game.add.tileSprite(0, 0, 512, 512, 'ground');
    	
    	this.music = this.game.add.audio('bgroundMusic',1,true);
        this.music.play('',0,1,true);

        this.bird = this.game.add.sprite(50, 245, 'bird');
        this.bird.body.gravity.y = 500; 
        this.bird.body.setCircle(16);
        this.bird.animations.add('fly');
        this.bird.play('fly', 15, true);
        
        var space_key = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space_key.onDown.add(this.jump, this);
        this.game.input.onDown.add(this.jump, this);
        this.initStarField();
        this.pipes = game.add.group();  
        this.pipes.createMultiple(20, 'wall');
       /* this.pipes.callAll('animations.add', 'animations', 'shine');
        this.pipes.callAll('play', null, 'shine', 15, true);
        this.pipes.callAll('bringToTop');*/
        
        //this.timer = this.game.time.events.loop(1500, this.add_row_of_pipes, this);  
        this.score = -1;  
        this.spawnWall = 180;
        var style = { font: "30px Arial", fill: "#ffffff" };  
        this.label_score = this.game.add.text(20, 20, "0", style);  
        
        this.bird.bringToTop();
        this.bird.anchor.setTo(-0.2, 0.5);  
    },
    
    update: function() {
    	this.background.tilePosition.x -= 1;
    	this.ground.tilePosition.x -= 4;

    	this.game.physics.overlap(this.bird, this.pipes, this.restart_game, null, this);  
        if (this.bird.inWorld == false)
            this.restart_game();
        if (this.bird.angle < 20)  
            this.bird.angle += 1;
        this.spawnWall --;
        if (this.spawnWall == 0){
        	this.spawnWall = 90;
        	this.add_row_of_pipes();
        }
    },
    
    // Make the bird jump 
    jump: function() {  
        // Add a vertical velocity to the bird
        this.bird.body.velocity.y = -250;
        this.game.add.tween(this.bird).to({angle: -20}, 100).start();  

    },

    // Restart the game
    restart_game: function() {  
    	//this.game.time.events.remove(this.timer);
    	this.music.stop();
        // Start the 'main' state, which restarts the game
        this.game.state.start('main');
    },
    
    add_one_pipe: function(x, y) {  
        var pipe = this.pipes.getFirstDead();
        if (!pipe)
        	return;
        pipe.reset(x, y);
        pipe.body.velocity.x = -200; 
        pipe.outOfBoundsKill = true;
    },
    
    add_row_of_pipes: function() {  
    	this.score += 1;  
    	this.label_score.content = this.score;
    	
        var hole = Math.floor(Math.random()*5)+1;

        for (var i = 0; i < 8; i++)
            if (i != hole && i != hole +1  && i != hole +2) 
                this.add_one_pipe(320, i*60+10);   
    },
    
    initStarField: function(){
        this.clouds = game.add.group();
    	for (var i = 0; i < 5; i++){
    		var cloudType = rand(1,2);
    		var cloud = this.clouds.create(rand(0,320), rand(0,300), 'cloud'+cloudType);
    		cloud.body.velocity.x = rand(50,100)*-1; 
    		cloud.outOfBoundsKill = true;
    		cloud.events.onKilled.add(function(cloud){
    			cloud.reset(320, rand(0,300));
    			cloud.body.velocity.x = rand(50,100)*-1;
    		});
    	}
    }
};

// Add and start the 'main' state to start the game
game.state.add('main', game_state.main);  
game.state.start('main'); 

function rand(a, b){
	return Math.floor(Math.random()*b)+a;
}