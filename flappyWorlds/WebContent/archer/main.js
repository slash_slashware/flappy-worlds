// Initialize Phaser, and creates a 400x490px game
var game = new Phaser.Game(320, 480, Phaser.AUTO, 'game_div');
var game_state = {};

// Creates a new 'main' state that wil contain the game
game_state.main = function() { };  
game_state.main.prototype = {

    preload: function() { 
		// Function called first to load all the assets
    	 // Change the background color of the game
        this.game.stage.backgroundColor = '#000000';

        // Load the bird sprite
        this.game.load.image('bird', 'assets/ship2.png'); 
        this.game.load.image('star', 'assets/star.png'); 
        //this.game.load.image('pipe', 'assets/mine.png');
        this.game.load.spritesheet('mine', 'assets/mine.png', 64, 64);
        this.game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL; //resize your window to see the stage resize too
        this.game.stage.scale.setShowAll();
        this.game.stage.scale.refresh();
    },

    create: function() { 
        this.bird = this.game.add.sprite(50, 245, 'bird');
        this.bird.body.gravity.y = 500;  
        var space_key = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space_key.onDown.add(this.jump, this);
        this.game.input.onDown.add(this.jump, this);
        this.initStarField();
        this.pipes = game.add.group();  
        this.pipes.createMultiple(20, 'mine');
        this.pipes.callAll('animations.add', 'animations', 'shine');
        this.pipes.callAll('play', null, 'shine', 15, true);
        this.pipes.callAll('bringToTop');
        
        this.timer = this.game.time.events.loop(1500, this.add_row_of_pipes, this);  
        this.score = 0;  
        var style = { font: "30px Arial", fill: "#ffffff" };  
        this.label_score = this.game.add.text(20, 20, "0", style);  
        
        this.bird.bringToTop();
        this.bird.anchor.setTo(-0.2, 0.5);  
    },
    
    update: function() {
    	this.game.physics.overlap(this.bird, this.pipes, this.restart_game, null, this);  
        if (this.bird.inWorld == false)
            this.restart_game();
        if (this.bird.angle < 20)  
            this.bird.angle += 1;
    },
    
    // Make the bird jump 
    jump: function() {  
        // Add a vertical velocity to the bird
        this.bird.body.velocity.y = -250;
        this.game.add.tween(this.bird).to({angle: -20}, 100).start();  

    },

    // Restart the game
    restart_game: function() {  
    	this.game.time.events.remove(this.timer);  
        // Start the 'main' state, which restarts the game
        this.game.state.start('main');
    },
    
    add_one_pipe: function(x, y) {  
        var pipe = this.pipes.getFirstDead();
        pipe.reset(x, y);
        pipe.body.velocity.x = -200; 
        pipe.outOfBoundsKill = true;
    },
    
    add_row_of_pipes: function() {  
    	// TODO: Only add score once the bird gets past the pipe
    	this.score += 1;  
    	this.label_score.content = this.score;
    	
        var hole = Math.floor(Math.random()*5)+1;

        for (var i = 0; i < 8; i++)
            if (i != hole && i != hole +1  && i != hole +2) 
                this.add_one_pipe(400, i*60+10);   
    },
    
    initStarField: function(){
        this.stars = game.add.group();
    	for (var i = 0; i < 20; i++){
    		var star = this.stars.create(rand(0,400), rand(0,490), 'star');
    		star.body.velocity.x = rand(50,100)*-1; 
    		star.outOfBoundsKill = true;
    		star.events.onKilled.add(function(star){
    			star.reset(400, rand(0,490));
    			star.body.velocity.x = rand(50,100)*-1;
    		});
    	}
    }
};

// Add and start the 'main' state to start the game
game.state.add('main', game_state.main);  
game.state.start('main'); 

function rand(a, b){
	return Math.floor(Math.random()*b)+a;
}